import { CommentsRepository } from "../../core/repositories/comments.repository";

export class AddCommentUseCase {
  async execute(nameUserComment, emailUserComment, bodyComment) {
    const repository = new CommentsRepository();
    const addedComment = await repository.addComment(
      nameUserComment,
      emailUserComment,
      bodyComment
    );
    return addedComment;
  }
}
