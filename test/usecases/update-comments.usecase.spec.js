import { CommentsRepository } from "../../src/core/repositories/comments.repository";
import { UpdateCommentsUseCase } from "../../src/core/usecases/update-comments.usecase";
import { COMMENTS } from "./../fixtures/comments";

jest.mock("../../src/core/repositories/comments.repository");

describe("Update comments use case", () => {
  beforeEach(() => {
    CommentsRepository.mockClear();
  });

  it("should update a comment", async () => {
    const updatedComment = {
      id: 1,
      name: "Updated name",
      email: "Updated email",
      body: "Updated body",
    };

    CommentsRepository.mockImplementation(() => {
      return {
        updateComment: (
          commentId,
          nameUserComment,
          emailUserComment,
          bodyComment
        ) => {
          return {
            ...COMMENTS[0],
            id: commentId,
            name: nameUserComment,
            email: emailUserComment,
            body: bodyComment,
          };
        },
      };
    });

    const useCase = new UpdateCommentsUseCase();
    const updateComment = await useCase.execute(
      updatedComment.id,
      updatedComment.name,
      updatedComment.email,
      updatedComment.body,
    );


    expect(updateComment.id).toEqual(updatedComment.id);
  });
});
