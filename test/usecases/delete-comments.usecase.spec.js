import { CommentsRepository } from "../../src/core/repositories/comments.repository";
import { DeleteCommentsUseCase } from "../../src/core/usecases/delete-comments.usecase";
import { COMMENTS } from "./../fixtures/comments";

jest.mock("../../src/core/repositories/comments.repository");

describe("Delete comments use case", () => {
  beforeEach(() => {
    CommentsRepository.mockClear();
  });

  it("should delete a comment", async () => {
    const commentIdToDelete = 1;
    let deletedComment = false;

    CommentsRepository.mockImplementation(() => {
      return {
        deleteComment: (commentId) => {
          if (commentId === commentIdToDelete) {
            deletedComment = true;
          }
        },
      };
    });

    const useCase = new DeleteCommentsUseCase();
    await useCase.execute(commentIdToDelete);

    expect(deletedComment).toEqual(true);
    expect(commentIdToDelete).not.toBe(
      expect.arrayContaining(COMMENTS.map((comment) => comment.id))
    );
  });
});
