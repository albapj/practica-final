/**
 * @typedef {Object} CommentType
 * @property {string} commentId
 * @property {string} title
 * @property {string} userEmail
 * @property {string} content
 */
export class Comment {
    /**
     * @constructor
     * @param {CommentType} comment
     */
    constructor({commentId, title, userEmail, content}) {
        this.commentId = commentId;
        this.title = title;
        this.userEmail = userEmail;
        this.content = content;
    }
}