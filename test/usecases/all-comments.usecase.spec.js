import { CommentsRepository } from "../../src/core/repositories/comments.repository";
import { AllCommentsUseCase } from "../../src/core/usecases/all-comments.usecase";
import { COMMENTS } from "./../fixtures/comments";

jest.mock("../../src/core/repositories/comments.repository");

describe("All comments use case", () => {
  beforeEach(() => {
    CommentsRepository.mockClear();
  });

  it("should get all comments", async () => {
    CommentsRepository.mockImplementation(() => {
      return {
        getAllComments: () => {
          return COMMENTS;
        },
      };
    });

    const comments = await AllCommentsUseCase.execute();

    expect(comments).toHaveLength(COMMENTS.length);
    expect(comments[0].body).toBe(COMMENTS[0].body);
  });
});
