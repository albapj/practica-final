import { CommentsRepository } from "../../core/repositories/comments.repository";

export class AllCommentsUseCase {
  static async execute() {
    const repository = new CommentsRepository();
    const comments = await repository.getAllComments();
    return comments;
  }
}
