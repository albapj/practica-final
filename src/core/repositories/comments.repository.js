import axios from "axios";

export class CommentsRepository {
  async getAllComments() {
    try {
      return await (
        await axios.get("https://jsonplaceholder.typicode.com/comments")
      ).data;
    } catch (error) {
      console.error("Error al mostrar los comentarios:", error.message);
      throw error;
    }
  }

  async deleteComment(commentId) {
    try {
      await axios.delete(
        `https://jsonplaceholder.typicode.com/comments/${commentId}`
      );
    } catch (error) {
      console.error("Error al eliminar el comentario:", error.message);
      throw error;
    }
  }

  async addComment(nameUserComment, emailUserComment, bodyComment) {
    try {
      await axios.post("https://jsonplaceholder.typicode.com/comments", {
        name: nameUserComment,
        email: emailUserComment,
        body: bodyComment,
      });
    } catch (error) {
      console.error("Error al añadir el comentario:", error.message);
      throw error;
    }
  }

  async updateComment(commentId, nameUserComment, emailUserComment, bodyComment) {
    try {
      await axios.put(`https://jsonplaceholder.typicode.com/comments/${commentId}`, {
        name: nameUserComment,
        email: emailUserComment,
        body: bodyComment,
      });
    } catch (error) {
      console.error("Error al actualizar el comentario:", error.message);
      throw error;
    }
  }
}
