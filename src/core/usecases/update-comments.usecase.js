import { CommentsRepository } from "../../core/repositories/comments.repository";

export class UpdateCommentsUseCase {
  async execute(commentId, nameUserComment, emailUserComment, bodyComment) {
    const repository = new CommentsRepository();
    const updatedComment = await repository.updateComment(commentId, nameUserComment, emailUserComment, bodyComment);
    return updatedComment;
  }
}
