import { CommentsRepository } from "../../src/core/repositories/comments.repository";
import { AddCommentUseCase } from "../../src/core/usecases/add-comments.usecase";
import { COMMENTS } from "./../fixtures/comments";

jest.mock("../../src/core/repositories/comments.repository");

describe("Add comments use case", () => {
  beforeEach(() => {
    CommentsRepository.mockClear();
  });

  it("should add a comment", async () => {
    const newNameUserComment = "New name";
    const newEmailUserComment = "New email";
    const newBodyComment = "New body";

    CommentsRepository.mockImplementation(() => {
      return {
        addComment: (name, email, body) => {
          return { id: COMMENTS.length + 1, name, email, body };
        },
      };
    });

    const useCase = new AddCommentUseCase();
    const newComment = await useCase.execute(
      newNameUserComment,
      newEmailUserComment,
      newBodyComment
    );

    expect(newComment.id).toBe(501);
    expect(newComment).toEqual({
      id: 501,
      name: newNameUserComment,
      email: newEmailUserComment,
      body: newBodyComment,
    });
  });
});
