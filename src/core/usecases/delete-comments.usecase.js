import { CommentsRepository } from "../../core/repositories/comments.repository";

export class DeleteCommentsUseCase {
  async execute(commentId) {
    const repository = new CommentsRepository();
    const deletedComment = await repository.deleteComment(commentId);
    return deletedComment;
  }
}
